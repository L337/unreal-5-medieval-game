// Fill out your copyright notice in the Description page of Project Settings.


#include "Cannon.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Kismet/GamePlayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Zombie.h"
#include "ProtectPoint.h"
#include "Projectile.h"
#include "Sound/SoundBase.h"
#include <limits>


// Sets default values
ACannon::ACannon()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>("CollisionBox");
	CollisionBox->SetupAttachment(RootComponent);

	Wheels = CreateDefaultSubobject<UStaticMeshComponent>("Wheels");
	Wheels->SetupAttachment(CollisionBox);

	Base = CreateDefaultSubobject<UStaticMeshComponent>("Base");
	Base->SetupAttachment(Wheels);

	Barrel = CreateDefaultSubobject<USkeletalMeshComponent>("Barrel");
	Barrel->SetupAttachment(Base);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>("ProjectileSpawn");
	ProjectileSpawnPoint->SetupAttachment(Barrel);

	FireRate = 2.f;

	Timer = FireRate;
	
	bGameOver = false;


	Range = 1000.f;

	RotationSpeedSet = 80.f;
}

void ACannon::GameOver()
{
	bGameOver = true;
}

// Called when the game starts or when spawned
void ACannon::BeginPlay()
{
	Super::BeginPlay();
	ProtectPoint = Cast<AProtectPoint>(UGameplayStatics::GetActorOfClass(GetWorld(), AProtectPoint::StaticClass()));
}

AZombie* ACannon::FindClosestEnemy()
{
	AZombie* ZombieToShootAt = nullptr;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AZombie::StaticClass(), Zombies);
	float shortest = Range;
	float big = 99999999999999999.f;
	for (int32 i = 0; i < Zombies.Num(); i++) 
	{
		AZombie* zombie = Cast<AZombie>(Zombies[i]);
		if (zombie->GetDead())
		{
			continue;
		}
		float d = Distance(zombie->GetActorLocation(), GetActorLocation());
		if (d < shortest)
		{
			if (ProtectPoint)
			{

				float ProtecDist = Distance(ProtectPoint->GetActorLocation(), zombie->GetActorLocation());
				if (ProtecDist < big)
				{
					big = ProtecDist;
				ZombieToShootAt = zombie;
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("ProtectPoint not Found!"))
			}
		}
	}
	return ZombieToShootAt;
}


float ACannon::Distance(FVector Location1, FVector Location2)
{
	FVector Difference = Location1 - Location2;
	float Distance = sqrt((Difference.X * Difference.X) + (Difference.Y * Difference.Y));
	return Distance;
}


void ACannon::SpawnProjectile()
{
	if (Projectile)
	{
		FActorSpawnParameters NormalParams;
		GetWorld()->SpawnActor<AProjectile>(Projectile, ProjectileSpawnPoint->GetComponentLocation(), ProjectileSpawnPoint->GetComponentRotation(), NormalParams);
	}

	else
	{
		UE_LOG(LogTemp, Error, TEXT("Projectile not found! Set projectile in blueprint"));
	}

	if (Particle)
	{
		FTransform Transform;
		Transform.SetLocation(ProjectileSpawnPoint->GetComponentLocation());
		Transform.SetScale3D(FVector3d(0.7f, 0.7f, 0.7f));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, Transform);
	}

	else
	{
		UE_LOG(LogTemp, Error, TEXT("Particle not found! Set particle in blueprint"));
	}
	if (CannonFire)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), CannonFire, ProjectileSpawnPoint->GetComponentLocation(),1.f,FMath::FRandRange(1.f, 1.3f),0.f,SoundDistance);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No sound cue found!"));
	}
}

void ACannon::RotateCannon(FVector LocationLook, float DeltaTime)
{
	FRotator ActorRot = GetActorRotation();
	FRotator LookAt = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), LocationLook);
	float BarrelPitch = Barrel->GetRelativeRotation().Pitch;
	float RotationSpeed = RotationSpeedSet;
	RotationSpeed *= DeltaTime;


	if (ActorRot.Yaw >= (LookAt.Yaw + RotationSpeed))
	{
		AddActorLocalRotation(FRotator(0.f, -RotationSpeed, 0.f));
	}
	if (ActorRot.Yaw <= (LookAt.Yaw - RotationSpeed))
	{
		AddActorLocalRotation(FRotator(0.f, RotationSpeed, 0.f));
	}
	if ( BarrelPitch <= (LookAt.Pitch - RotationSpeed))
	{
		Barrel->AddRelativeRotation(FRotator(RotationSpeed, 0.f, 0.f));
	}
	if (BarrelPitch >= (LookAt.Pitch + RotationSpeed))
	{
		Barrel->AddRelativeRotation(FRotator(-RotationSpeed, 0.f, 0.f));
	}
}

bool ACannon::RayCast()
{
	FVector Start = ProjectileSpawnPoint->GetComponentLocation();
	FVector End = Start + (Barrel->GetForwardVector() * 1000);
	FHitResult ObjectHit;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);


	if (GetWorld()->LineTraceSingleByChannel(ObjectHit, Start, End, ECollisionChannel::ECC_Visibility, Params, FCollisionResponseParams()))
	{
		if (Cast<AZombie>(ObjectHit.GetActor()))
		{
			return true;
		}
		else
		{
			return false;;
		}
	}
	else
	{
		return false;
	}
}

// Called every frame
void ACannon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	AZombie* zombie = FindClosestEnemy();
	if (zombie && !bGameOver)
	{
		RotateCannon(zombie->GetActorLocation(), DeltaTime);

		if (Timer >= FireRate && RayCast())
		{
			Timer = 0.f;
			bFire = true;
			SpawnProjectile();
		}
	}
	if (!bGameOver && Timer < FireRate)
	{
		Timer += DeltaTime;
		bFire = false;
	}
}

// Called to bind functionality to input
void ACannon::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

