// Fill out your copyright notice in the Description page of Project Settings.


#include "ProtectPoint.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FPCharacter.h"
#include "Zombie.h"
#include "Cannon.h"

// Sets default values
AProtectPoint::AProtectPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);

	Box = CreateDefaultSubobject<UBoxComponent>("Box");
	Box->SetupAttachment(Mesh);

	bGameOver = false;

	Health = 100.f;

}

void AProtectPoint::TakeDamage(float AttackDamage)
{
	if (!bGameOver)
	{
	Health -= AttackDamage;

	}
	if (Health <= 0.f)
	{
		GameOver();
		bGameOver = true;
	}

}

void AProtectPoint::GameOver()
{
	Player = Cast<AFPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Player)
	{
		Player->GameOver();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Player Pawn Not Found! Game Over not called"))
	}
	ACannon* Cannon;
	TArray<AActor*> Cannons;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACannon::StaticClass(), Cannons);
	for (int32 i = 0; i < Cannons.Num(); i++)
	{
		Cannon = Cast<ACannon>(Cannons[i]);
		if (Cannon)
		{
			Cannon->GameOver();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Actor is not a cannon in array! Game over not called for cannons"))
		}
	}

}

// Called when the game starts or when spawned
void AProtectPoint::BeginPlay()
{
	Super::BeginPlay();
	
	Box->OnComponentBeginOverlap.AddDynamic(this, &AProtectPoint::OnOverlapBegin);

}

void AProtectPoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AZombie* Zombie = Cast<AZombie>(OtherActor);
	if (Zombie)
	{
		Zombie->SetAttack();
	}
}

// Called every frame
void AProtectPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

