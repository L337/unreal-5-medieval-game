// Fill out your copyright notice in the Description page of Project Settings.


#include "ZombieSpawner.h"
#include "Zombie.h"
#include "Components/BoxComponent.h"
//#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"

// Sets default values
AZombieSpawner::AZombieSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	SpawnVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnVolume"));
	SpawnVolume->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AZombieSpawner::BeginPlay()
{
	Super::BeginPlay();
	LastSpawnTime = GetWorld()->GetTimeSeconds();
}

// Called every frame
void AZombieSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float CurrentTime = GetWorld()->GetTimeSeconds();
	if (CurrentTime - LastSpawnTime > SPAWN_FREQUENCY_SECONDS) {
		LastSpawnTime = CurrentTime;
		if (SpawningEnabled) {
			SpawnZombie();
		}
	}
}

// Creates a new Zombie in a randomized location within the bounds of the SpawnVolume.
// The spawned zombie will be aligned with visible ground (make sure the SpawnVolume is above ground).
void AZombieSpawner::SpawnZombie()
{
	// Randomly choose a location at the top of the box.
	FVector Origin;
	FVector BoxExtents;
	GetActorBounds(false, Origin, BoxExtents, false);
	FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtents) * FVector(1, 1, 0);
	FRotator SpawnRotation = GetActorRotation();

	// Spawn the Zombie at the top of the box.
	FActorSpawnParameters SpawnParams;
	FTransform SpawnTransform = FTransform(SpawnRotation, SpawnLocation, FVector(1, 1, 1));
	AZombie* zom = GetWorld()->SpawnActor<AZombie>(ZombieBP, SpawnTransform, SpawnParams);
	if (zom == nullptr) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("ZombieSpawner: SpawnActor returned a null pointer!"));
		UE_LOG(LogTemp, Warning, TEXT("ZombieSpawner: SpawnActor returned a null pointer!"));
		return;
	}

	// Raycast from the top of the box to a point "very" far below.
	FHitResult OutHit;
	FVector Start = FVector(SpawnLocation.X, SpawnLocation.Y, (Origin.Z + BoxExtents.Z));
	FVector End = Start - FVector(0, 0, 10000);
	ECollisionChannel TraceChannel = ECC_Visibility;
	FCollisionQueryParams Params;
	FCollisionResponseParams ResponseParam;
	bool LineTraceSucceeded = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, TraceChannel, Params, ResponseParam);
	if (!LineTraceSucceeded) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("ZombieSpawn Line Trace failed to find anything visible below it! The spawn point might be too low."));
		UE_LOG(LogTemp, Warning, TEXT("ZombieSpawn Line Trace failed to find anything visible below it! The spawn point might be too low."));
		return;
	}

	// Move the zombie to the ground, and rotates it accordingly.
	// This is placed here because you need to spawn the zombie before retrieving its bounds.
	FVector ZombieOrigin;
	FVector ZombieBoxExtents;
	zom->GetActorBounds(false, ZombieOrigin, ZombieBoxExtents, false);
	SpawnLocation = OutHit.Location + ZombieBoxExtents.Z * OutHit.Normal;
	// zom->SetActorRotation(UKismetMathLibrary::MakeRotFromZ(OutHit.Normal));
	zom->SetActorLocation(SpawnLocation);
}