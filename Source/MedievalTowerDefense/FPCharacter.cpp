// Fill out your copyright notice in the Description page of Project Settings.


#include "FPCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"
#include "Cannon.h"
#include "Zombie.h"
#include "Kismet/GameplayStatics.h"
#include "ExplosionProjectile.h"
#include "Engine/EngineTypes.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
AFPCharacter::AFPCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creates object for the camera
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(RootComponent);

	// Attaches Mesh to Camera
	MeshRef = GetMesh();
	GetMesh()->SetupAttachment(Camera);

	FireBallSpawn = CreateDefaultSubobject<USceneComponent>("Fire Ball Spawn");
	FireBallSpawn->SetupAttachment(Camera);

	// Reference to the character movement component
	MovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent());

	//Floats
	NormalWalkSpeed = 600.f;
	MovementComponent->MaxWalkSpeed = NormalWalkSpeed;
	SprintSpeed = 1.5 * NormalWalkSpeed;
	CrouchSlideTime = 0.4f;
	CrouchSlideTimeElapsed = CrouchSlideTime;
	NormalCrouchSpeed = MovementComponent->MaxWalkSpeedCrouched;
	AttackTimer = 0.f;
	AttackTimer2 = 0.f;
	Points = 40;
	RayHitLocation = FVector();
	ZombiesKilled = 0;
	Damage = 100.f;
	ImpulseAmount = 2000.f;
	Mana = 100.f;
	SwordTimer = 0.f;

	// Booleans
	bCrouching = false;
	bSprinting = false;
	bAttack = false;
	bPickupCannon = false;
	bGameOver = false;
	bMagicHeld = false;
	bTeleportSpell = true;
	bFireBallSpell = false;
	bPlayFootStep = true;
	bhasTeleportMana = false;

} 



void AFPCharacter::GameOver()
{
	bGameOver = true;
	DestroyPlayerInputComponent();
}

void AFPCharacter::TimeElapsed(float DeltaTime)
{
	if (!bGameOver)
	{
		Time += DeltaTime;
	}
}

// Called when the game starts or when spawned
void AFPCharacter::BeginPlay()
{
	Super::BeginPlay();

	MeshRef->OnComponentBeginOverlap.AddDynamic(this, &AFPCharacter::OnOverlapBegin);
	
}


// Called every frame
void AFPCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CharacterAttackTimer(DeltaTime);

	RayCast();

	TimeElapsed(DeltaTime);

	PlayFootSteps(DeltaTime);

	RegenMana(DeltaTime);

	if (SwordTimer >= 0.f)
	{
		SwordTimer -= DeltaTime;
	}
}

// Called to bind functionality to input
void AFPCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AFPCharacter::CrouchPressed);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AFPCharacter::CrouchReleased);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AFPCharacter::SprintPressed);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AFPCharacter::SprintReleased);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AFPCharacter::AttackPressed);
	PlayerInputComponent->BindAxis("MoveForward", this, &AFPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveSide", this, &AFPCharacter::MoveSide);
	PlayerInputComponent->BindAxis("LookSide", this, &AFPCharacter::LookSide);
	PlayerInputComponent->BindAxis("LookUp", this, &AFPCharacter::LookUp);
	PlayerInputComponent->BindAction("PickUp",IE_Pressed, this, &AFPCharacter::PickupCannon);
	PlayerInputComponent->BindAction("Place", IE_Pressed, this, &AFPCharacter::SpawnCannons);
	PlayerInputComponent->BindAction("MagicCast", IE_Pressed, this, &AFPCharacter::MagicPressed);
	PlayerInputComponent->BindAction("MagicCast", IE_Released, this, &AFPCharacter::MagicReleased);
	PlayerInputComponent->BindAction("TeleportSelect", IE_Pressed, this, &AFPCharacter::SetTeleport);
	PlayerInputComponent->BindAction("FireBallSelect", IE_Pressed, this, &AFPCharacter::SetFireBall);
}

void AFPCharacter::MoveForward(float value)
{
	AddMovementInput(GetActorForwardVector() * value);
}

void AFPCharacter::MoveSide(float value)
{
	AddMovementInput(GetActorRightVector() * value);
}

void AFPCharacter::LookSide(float value)
{
	AddControllerYawInput(value);
}

void AFPCharacter::LookUp(float value)
{
	AddControllerPitchInput(-value);
}


void AFPCharacter::CrouchPressed()
{
	bCrouching = true;
	CrouchSlideTimeElapsed = 0.f;
	Crouch();
}

void AFPCharacter::CrouchReleased()
{
	bCrouching = false;
	UnCrouch();
}

void AFPCharacter::SprintPressed()
{
	bSprinting = true;
	MovementComponent->MaxWalkSpeed = SprintSpeed;
}

void AFPCharacter::SprintReleased()
{
	bSprinting = false;
	MovementComponent->MaxWalkSpeed = NormalWalkSpeed;
}

void AFPCharacter::AttackPressed()
{
	bAttack = true;
	bCanSwing = true;

	if (AttackTimer <= 0.f)
	{
		AttackTimer = 0.4f;
	}
	if (AttackTimer2 <= 0.f)
	{
		AttackTimer2 = 0.2f;
	}
	
}

void AFPCharacter::CharacterAttackTimer(float DeltaTime)
{
	
	if (AttackTimer > 0.f)
	{
		AttackTimer -= DeltaTime;
	}
	if (AttackTimer <= 0.f && bAttack)
	{
		bAttack = false;
	}
	if (AttackTimer2 > 0.f)
	{
		AttackTimer2 -= DeltaTime;
	}
	if (AttackTimer2 <= 0.f && bCanSwing)
	{
		bCanSwing = false;
	}
}

void AFPCharacter::PlayFootSteps(float DeltaTime)
{

	if (GetVelocity().X != 0.f || GetVelocity().Y != 0.f && GetVelocity().Z == 0.f)
	{
		if (FootStepsTimer <= 0.f)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), FootStep,1.4f);
			if (bSprinting)
			{
				FootStepsTimer = 0.25f;
			}
			else
			{
				FootStepsTimer = .45f;
			}
		}
		else
		{
			FootStepsTimer -= DeltaTime;
		}

	}
	else
	{
		FootStepsTimer = 0.f;
	}

}

void AFPCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AZombie* Zombie = Cast<AZombie>(OtherActor);
	if (Zombie && bAttack)
	{
		if (!SwordHitSound)
		{
			UE_LOG(LogTemp, Warning, TEXT("Sword Hit Sound not set!"));
		}
		if (!Zombie->GetDead() && SwordTimer <= 0.f)
		{
			SwordTimer = 0.15f;
			Zombie->TakeDamage(Damage, ImpulseAmount,GetActorLocation());
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SwordHitSound, Zombie->GetActorLocation(),0.7f,FMath::FRandRange(0.8f,1.f),0.f,SoundDistance);
		}
	}
}

void AFPCharacter::PickupCannon()
{
	if (Cannon && bPickupCannon && !bMagicHeld)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), CannonDestroy, RayHitLocation);
		Cannon->Destroy();
		Points += 20;
	}
}

void AFPCharacter::SpawnCannons()
{
	if (Points >= 20 && ObjectHit.bBlockingHit && !bMagicHeld)
	{
		FRotator CannonRotator = UKismetMathLibrary::MakeRotFromZ(ObjectHit.Normal);
		ACannon* Temp = GetWorld()->SpawnActor<ACannon>(SpawnCannon, RayHitLocation, CannonRotator);
		if (Temp)
		{
			Points -= 20;
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), CannonCreate, RayHitLocation);
		}
	}
}

void AFPCharacter::RayCast()
{
	float CastDist;
	if (bMagicHeld && bTeleportSpell)
	{
		CastDist = 3000.f;
	}
	else
	{
		CastDist = 400.f;
	}

	FVector Start = Camera->GetComponentLocation();
	FVector End = Start + Camera->GetForwardVector() * CastDist;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(ObjectHit, Start, End, ECollisionChannel::ECC_Visibility, Params, FCollisionResponseParams()))
	{
		bCanTeleport = true;
		Cannon = Cast<ACannon>(ObjectHit.GetActor());
		RayHitLocation = ObjectHit.ImpactPoint;

		if (bMagicHeld && TeleportEffectRef && bTeleportSpell)
		{
			TeleportEffectRef->SetActorLocation(ObjectHit.ImpactPoint);
		}

		if (Cannon && !bMagicHeld)
		{

			bPickupCannon = true;
		}
		else
		{
			Cannon = nullptr;
			bPickupCannon = false;
		}
	}
	else
	{
		bPickupCannon = false;
		bCanTeleport = false;
	}
}

void AFPCharacter::SetTeleport()
{
	if (!bMagicHeld)
	{
		bTeleportSpell = true;
		bFireBallSpell = false;
	}
}

void AFPCharacter::SetFireBall()
{
	if (!bMagicHeld)
	{
		bFireBallSpell = true;
		bTeleportSpell = false;
	}
}

void AFPCharacter::RegenMana(float DeltaTime)
{
	UE_LOG(LogTemp, Warning, TEXT("Mana: %f"), Mana);
	if (Mana < 100.f)
	{
		Mana += DeltaTime;
	}
}

void AFPCharacter::MagicPressed()
{
	bMagicHeld = true;
	if (bTeleportSpell && Mana >= 10.f)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), TeleportCharge);
		FActorSpawnParameters NormalParams;
		TeleportEffectRef = GetWorld()->SpawnActor<AActor>(TeleportEffect, RayHitLocation, FRotator(), NormalParams);
		Mana -= 10.f;
		bhasTeleportMana = true;
	}
	else
	{
		bhasTeleportMana = false;
	}
}

void AFPCharacter::MagicReleased()
{
	bMagicHeld = false;
	if (bTeleportSpell && bhasTeleportMana)
	{
		TeleportEffectRef->Destroy();
		FVector TPLocation = RayHitLocation;
		TPLocation.Z += GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

		if (bCanTeleport)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), Teleport);
			TeleportTo(TPLocation,GetActorRotation());
		}
	}
	else
	{
		if (TeleportEffectRef)
		{
			TeleportEffectRef->Destroy();
		}
	}
	if (bFireBallSpell && Mana >= 40.f)
	{
		Mana -= 40.f;
		UGameplayStatics::PlaySound2D(GetWorld(), FireBallSound);
		GetWorld()->SpawnActor<AExplosionProjectile>(FireBall, FireBallSpawn->GetComponentLocation(), FireBallSpawn->GetComponentRotation());
	}
}

void AFPCharacter::CrouchSlide(float DeltaTime)
{
	if (CrouchSlideTimeElapsed < CrouchSlideTime && bSprinting && bCrouching)
	{
		MovementComponent->MaxWalkSpeedCrouched = FMath::Lerp(SprintSpeed, NormalCrouchSpeed, CrouchSlideTimeElapsed / CrouchSlideTime);
		CrouchSlideTimeElapsed += DeltaTime;
	}
}



