// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Cannon.generated.h"

UCLASS()
class MEDIEVALTOWERDEFENSE_API ACannon : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACannon();

	void GameOver();

	UFUNCTION(BlueprintCallable)
	bool GetFire() { return bFire; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	class USkeletalMeshComponent* Barrel;

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* Base;

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* Wheels;

	UPROPERTY(VisibleAnywhere)
	USceneComponent* ProjectileSpawnPoint;

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* CollisionBox;

	class AProtectPoint* ProtectPoint;

	TArray<AActor*> Zombies;

	class AZombie* FindClosestEnemy();

	float Distance(FVector Location1, FVector Location2);

	void RotateCannon(FVector LocationLook, float DeltaTime);

	bool RayCast();


	UFUNCTION()
	void SpawnProjectile();

	UPROPERTY(EditAnywhere, Category = "Projectile")
	TSubclassOf<class AProjectile> Projectile;

	UPROPERTY(EditAnywhere)
	UParticleSystem* Particle;

	UPROPERTY(EditAnywhere)
	class USoundBase* CannonFire;

	UPROPERTY(EditAnywhere)
	float Range;

	UPROPERTY(EditAnywhere)
	float FireRate;

	UPROPERTY(EditAnywhere)
	float RotationSpeedSet;

	UPROPERTY(EditAnywhere)
	USoundAttenuation* SoundDistance;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	float Timer;
	float Timer2;
	bool bGameOver;
	bool bFire;
};
