// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Zombie.h"
#include "ZombieSpawner.generated.h"

UCLASS()
class MEDIEVALTOWERDEFENSE_API AZombieSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZombieSpawner();

	UFUNCTION(BlueprintCallable)
	void SpawnZombie();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* SpawnVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SPAWN_FREQUENCY_SECONDS = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool SpawningEnabled = true;

	UPROPERTY(EditAnywhere, Category = "ActorSpawning")
	TSubclassOf<AZombie> ZombieBP;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	float LastSpawnTime;
};
