// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPCharacter.generated.h"

UCLASS()
class MEDIEVALTOWERDEFENSE_API AFPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPCharacter();

	UFUNCTION(BlueprintCallable)
	float GetMana() { return Mana; }
	
	UFUNCTION(BlueprintCallable)
	bool GetAttack() { return bAttack; }

	UFUNCTION(BlueprintCallable)
	bool GetCanSwing() { return bCanSwing; }

	UFUNCTION(BlueprintCallable)
	bool GetBlock() { return bBlock; }

	UFUNCTION(BlueprintCallable)
	int32 GetPoints() { return Points; }

	UFUNCTION(BlueprintCallable)
	int32 GetZombiesKilled() { return ZombiesKilled; }

	UFUNCTION(BlueprintCallable)
	bool GetPickup() { return bPickupCannon; }

	UFUNCTION(BlueprintCallable)
	bool GetGameOver() { return bGameOver; }

	UFUNCTION(BlueprintCallable)
	float GetTime() { return Time; }



	UPROPERTY(EditAnywhere)
	float Damage;

	UPROPERTY(EditAnywhere)
	USoundAttenuation* SoundDistance;

	void AddPoints() { Points++; }

	void AddZombiesKilled() { ZombiesKilled++; }

	void GameOver();

	void TimeElapsed(float DeltaTime);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float Mana;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere)
	USceneComponent* FireBallSpawn;

	UPROPERTY(VisibleAnywhere)
	class USkeletalMeshComponent* MagicArm;

	class USkeletalMeshComponent* MeshRef;

	class UCharacterMovementComponent* MovementComponent;

	void MoveForward(float value);
	void MoveSide(float value);
	void LookSide(float value);
	void LookUp(float value);
	void MagicPressed();
	void MagicReleased();
	void CrouchPressed();
	void CrouchReleased();
	void SprintPressed();
	void SprintReleased();
	void AttackPressed();
	void PickupCannon();
	void SpawnCannons();
	void RayCast();
	void SetTeleport();
	void SetFireBall();
	void RegenMana(float DeltaTime);

	void CrouchSlide(float DeltaTime);

	void CharacterAttackTimer(float DeltaTime);

	void PlayFootSteps(float DeltaTime);

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ACannon> SpawnCannon;

	UPROPERTY(EditAnywhere)
	USoundBase* SwordHitSound;

	UPROPERTY(EditAnywhere)
	USoundBase* FireBallSound;

	UPROPERTY(EditAnywhere)
	USoundBase* CannonDestroy;

	UPROPERTY(EditAnywhere)
	USoundBase* CannonCreate;

	UPROPERTY(EditAnywhere)
	USoundBase* Teleport;

	UPROPERTY(EditAnywhere)
	USoundBase* TeleportCharge;


	UPROPERTY(EditAnywhere)
	USoundBase* FootStep;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> TeleportEffect;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AExplosionProjectile> FireBall;

	AActor* TeleportEffectRef;

	UPROPERTY(EditAnywhere)
	float ImpulseAmount;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	bool bCrouching;
	bool bSprinting;
	bool bBlock;
	bool bPickupCannon;
	bool bCanSwing;
	bool bAttack;
	bool bMagicCast;
	bool bMagicHeld;
	bool bCanTeleport;
	bool bTeleportSpell;
	bool bFireBallSpell;
	bool bPlayFootStep;
	bool bhasTeleportMana;

	float CrouchStartSpeed;
	float CrouchEndSpeed;
	float SprintSpeed;
	float NormalWalkSpeed;
	float FootStepsTimer;
	float SwordTimer;

	float CrouchSlideTimeElapsed;
	float CrouchSlideTime;
	float NormalCrouchSpeed;
	float AttackTimer;
	float AttackTimer2;
	float Time;
	int32 Points;
	int32 ZombiesKilled;

	class ACannon* Cannon;
	
	FVector RayHitLocation;
	FHitResult ObjectHit;

	bool bGameOver;

};
