// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Zombie.generated.h"

UCLASS()
class MEDIEVALTOWERDEFENSE_API AZombie : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AZombie();

	UFUNCTION(BlueprintCallable)
	bool GetAttack() { if (!bDead)return bZomAttack; else return false; }

	UFUNCTION(BlueprintCallable)
	void SetAttack();

	UFUNCTION(BlueprintCallable)
	bool GetDead() { return bDead; }

	void TakeDamage(float TakeDamage, float Force, FVector Location);

	void KillZombie(float Force, FVector Location);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class UCapsuleComponent* CapsuleRef;

	class USkeletalMeshComponent* BodyMesh;

	class AProtectPoint* ProtectPoint;

	class AFPCharacter* MainCharacter;

	class AEnemyAI* AIRef;

	void GiveForce(float Force, FVector Location);

	UPROPERTY(VisibleAnywhere)
	USceneComponent* BloodSpawn;

	void Attack(float DeltaTime);

	UPROPERTY(EditAnywhere, Category = "Particle Effect")
	UParticleSystem* Particle;

	UPROPERTY(EditAnywhere)
	float Damage;

	UPROPERTY(EditAnywhere)
	float Health;

	UPROPERTY(EditAnywhere)
	float BlendWeight;

	UPROPERTY(EditAnywhere)
	USoundBase* DeathRattle;

	UPROPERTY(EditAnywhere)
	USoundBase* AttackRoar;

	UPROPERTY(EditAnywhere)
	USoundAttenuation* SoundDistance;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	float LifeSpan;
	bool bDead;
	bool bZomAttack;
	float Timer;
	bool bMoving;
};
