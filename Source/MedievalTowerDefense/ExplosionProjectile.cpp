// Fill out your copyright notice in the Description page of Project Settings.


#include "ExplosionProjectile.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Zombie.h"
#include "FPCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AExplosionProjectile::AExplosionProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = SceneRoot;

	Sphere = CreateDefaultSubobject<USphereComponent>("Collison Sphere");
	Sphere->SetupAttachment(SceneRoot);


	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("Projectile Movement");

	bExpansion = false;
	bOnce = false;
	Damage = 100.f;
	ExplosionSize = 100.f;
	Size = ExplosionSize;
	Scale = 32.f;
	ImpulseAmount = 40000.f;
}

// Called when the game starts or when spawned
void AExplosionProjectile::BeginPlay()
{
	Super::BeginPlay();
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AExplosionProjectile::OnOverlapBegin);
	Sphere->OnComponentHit.AddDynamic(this, &AExplosionProjectile::OnProjectileHit);

	SetLifeSpan(7.f);
}

void AExplosionProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFPCharacter* Character = Cast<AFPCharacter>(OtherActor);

	if (Character)
	{
		return;
	}

	bExpansion = true;
	ProjectileMovement->Deactivate();
	//ProjectileMovement->LimitVelocity(FVector(0.f, 0.f, 0.f));

	AZombie* Zombie = Cast<AZombie>(OtherActor);
	if (Zombie)
	{
		Zombie->TakeDamage(Damage, ImpulseAmount, GetActorLocation());
	}
}

void AExplosionProjectile::OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	bExpansion = true;
	ProjectileMovement->LimitVelocity(FVector(0.f, 0.f, 0.f));
}

// Called every frame
void AExplosionProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bExpansion)
	{
		if (!bOnce)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation(), 1.f, FMath::FRandRange(0.9f, 1.1f), 0.f, SoundDistance);
			FTransform Transform;
			Transform.SetLocation(GetActorLocation());
			Transform.SetScale3D(FVector3d(2.f, 2.f, 2.f));
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Explosion, Transform);
			SetLifeSpan(0.15f);
			bOnce = true;

		}

		Scale += DeltaTime * ExplosionSize * ExplosionSize;
		Sphere->SetSphereRadius(Scale);

	}
}

