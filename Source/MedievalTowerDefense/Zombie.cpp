// Fill out your copyright notice in the Description page of Project Settings.



#include "Zombie.h"
#include "Components/CapsuleComponent.h"
#include "FPCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"
#include "ProtectPoint.h"
#include "Kismet/KismetMathLibrary.h"
#include "FPCharacter.h"
#include "EnemyAI.h"



// Sets default values
AZombie::AZombie()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleRef = GetCapsuleComponent();

	BodyMesh = GetMesh();

	BloodSpawn = CreateAbstractDefaultSubobject<USceneComponent>("Blood Spawn");
	BloodSpawn->SetupAttachment(BodyMesh);

	Timer = 0.f;
	LifeSpan = 5.f;
	BlendWeight = 1.f;
	Health = 100.f;
	bZomAttack = false;
	bDead = false;
	Damage = 5.f;
	bMoving = false;
}

// Called when the game starts or when spawned
void AZombie::BeginPlay()
{
	Super::BeginPlay();
	
	ProtectPoint = Cast<AProtectPoint>(UGameplayStatics::GetActorOfClass(GetWorld(), AProtectPoint::StaticClass()));

	AIRef = Cast<AEnemyAI>(GetController());

}

// Called every frame
void AZombie::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!bDead && bZomAttack)
	{
		Attack(DeltaTime);
	}

	if (!bDead && !bZomAttack && AIRef && GetVelocity() == FVector(0.f, 0.f, 0.f))
	{
		AIRef->MoveToLocation(ProtectPoint->GetActorLocation(), 100.f);
		bMoving = true;
	}


}

// Called to bind functionality to input
void AZombie::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AZombie::Attack(float DeltaTime)
{
	FVector ProtecLoc = ProtectPoint->GetActorLocation();
	FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ProtecLoc);
	Rotation.Pitch = GetActorRotation().Pitch;
	SetActorRotation(Rotation);

	float Frequency = 2.f;

	if (bZomAttack && !bDead)
	{
		if (Timer < Frequency)
		{
			Timer += DeltaTime;
		}
		if (Timer >= Frequency)
		{
			Timer = 0.f;

			if (ProtectPoint)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), AttackRoar, BloodSpawn->GetComponentLocation());
				ProtectPoint->TakeDamage(Damage);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("Protect Point not Found. Damage will not be applied"));
			}
		}
	}
}


void AZombie::SetAttack()
{

	bZomAttack = true;
}

void AZombie::TakeDamage(float TakeDamage,float Force, FVector Location)
{
	Health -= TakeDamage;
	FTransform Transform;
	Transform.SetLocation(BloodSpawn->GetComponentLocation());
	Transform.SetRotation(BloodSpawn->GetComponentRotation().Quaternion());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, Transform);
	if (Health <= 0.f)
	{
		KillZombie(Force, Location);
	}
}

void AZombie::KillZombie(float Force, FVector Location)
{
	if (!bDead)
	{
		BodyMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		BodyMesh->SetSimulatePhysics(true);
		BodyMesh->SetPhysicsBlendWeight(BlendWeight);
		SetLifeSpan(LifeSpan);
		bDead = true;
		CapsuleRef->SetCollisionProfileName("Overlap All");
		CapsuleRef->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		MainCharacter = Cast<AFPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

		if (MainCharacter)
		{
			MainCharacter->AddPoints();
			MainCharacter->AddZombiesKilled();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Player Pawn not Found! Points have not been added!"))
		}
		if (DeathRattle)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathRattle, BloodSpawn->GetComponentLocation(), 1.f,FMath::FRandRange(0.8f,1.3f),0.f,SoundDistance);
		}
	}
	GiveForce(Force, Location);
}

void AZombie::GiveForce(float Force, FVector Location)
{
	FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Location);
	Rotation.Yaw *= -1.f;
	Rotation.Pitch *= -1.f;
	Rotation.Roll *= -1.f;
	FVector Vector = GetActorLocation();
	Vector += Force * Rotation.Vector();
	BodyMesh->AddImpulse(Vector);
}



